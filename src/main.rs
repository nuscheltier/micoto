use std::io;
use std::fs;
use std::io::prelude::*;
use clap::Parser;

#[derive(Clone)]
struct Entry {
    id: u32,
    title: String,
    checked: bool
}

impl Entry {
    fn to_cmd_string(&self) {
        let check = self.parse_checked();
        println!("{} - {} - [{}]", self.id, self.title, check)
    }

    fn to_csv(&self) -> String {
        let check = self.parse_checked();
        format!("{},{}", self.title, check)
    }
    fn parse_checked(&self) -> &str {
        if self.checked { "x" } else { " " }
    }
}

enum Output {
    Item(Entry),
    Text(String),
    Info(Entry),
    Help
}

#[derive(Parser, Debug)]
#[command(version)]
struct CLArguments {
    #[arg(short, long)]
    ///How many entries are shown per page
    page_length: Option<u32>,
    #[arg(short, long)]
    ///How wide the terminal is
    width: Option<u32>
}

struct Options {
    page_length: Option<u32>,
    offset: Option<u32>, //offset to be used for length of list
    width: Option<u32>
}

fn main() -> io::Result<()> {
    let commandline_arguments = CLArguments::parse();
    if commandline_arguments.page_length.is_some_and(|x| x < 1) {
        println!("Please be advised that a page length smaller than 1 is impossible to use.");
        return Ok(());
    }
    if commandline_arguments.width.is_some_and(|x| x < 15) {
        println!("While it might be possible to use micoto with a smaller width than 15 characters it is not advised. Please use more characters for width.");
        return Ok(());
    }
    let mut options = Options {
        page_length: commandline_arguments.page_length,
        offset: None,
        width: commandline_arguments.width
    };
    if commandline_arguments.page_length.is_some() {
        options.offset = Some(0); 
    }
    let mut database = load_database(&options)?;
    show_list(&database, &options);
    output_text(&options, Output::Help);
    loop {
        let mut command = String::new();
        io::stdin().read_line(&mut command)?;
        let commands_result = commands(command.trim(), &mut database, &mut options);
        if commands_result.is_ok() && commands_result.unwrap() { break; }
    }
    Ok(())
}

fn commands(command: &str, database: &mut Vec<Entry>, options: &mut Options) -> io::Result<bool>{
    match command {
        "c" => {
            id_input(database, &options, &check_command)?;
        },
        "n" => {
            output_text(&options, Output::Text("Title for new item:".to_string()));
            let mut item = String::new();
            io::stdin().read_line(&mut item)?;
            let entry = Entry {
                id: (database.len() as u32) + 1,
                title: item.trim().to_string(),
                checked: false
            };
            entry.to_cmd_string();
            database.push(entry);
        },
        "u" => {
            id_input(database, &options, &update_command)?;
        },
        "d" => {
            id_input(database, &options, &delete_command)?;
        },
        "i" => {
            id_input(database, &options, &info_command)?;
        },
        "m" => {
            move_command(database, &options)?;
            show_list(database, &options);
        },
        "h" => help_commands_explained(&options),
        "v" => {
            show_list(database, &options);
        },
        "s" => {
            println!("Saving list.");
            save_database(database, &options);
        },
        "l" => {
            println!("Reloading list.");
            if let Ok(db) = load_database(&options) { *database = db; }
            show_list(database, &options);
        },
        "q" => {
            output_text(&options, Output::Text("Before you go, do you want to save? [y/N/c]".to_string()));
            let mut input = String::new();
            io::stdin().read_line(&mut input)?;
            match input.trim() {
                "y" => save_database(database, &options),
                "c" => {
                    println!("-----");
                    return Ok(false);
                },
                _ => {}
            }
            println!("Bye.");
            return Ok(true);
        },
        input => {
            if input.starts_with('p') && options.page_length.is_some() {
                let mut input_string = String::from(input);
                input_string.remove(0);
                let page = input_string.parse::<u32>();
                if page.is_err() || page == Ok(0) {
                    output_text(&options, Output::Text("Not a valid page. Please use a number after [p].".to_string()));
                    return Ok(false);
                }
                if (database.len() as u32 / options.page_length.unwrap()) < page.clone().unwrap() &&
                    database.len() as u32 % options.page_length.unwrap() == 0 {
                    println!("Page not in range.");
                    return Ok(false);
                }
                options.offset = Some(page.unwrap() - 1);
                show_list(database, &options);
                help_commands(&options);
                return Ok(false);
            }
            output_text(&options, Output::Text("Command not recognized.".to_string()));
            output_text(&options, Output::Help);
        }
    }
    println!("-----");
    Ok(false)
}

fn id_input(database: &mut Vec<Entry>, options: &Options, f: &dyn Fn(&mut Vec<Entry>, &Options, u32) -> io::Result<()>) -> io::Result<()> {
    output_text(&options, Output::Text("Please input the id for the item to check out. ([c]ancel)".to_string()));
    loop {
        let mut id_string = String::new();
        io::stdin().read_line(&mut id_string)?;
        if id_string.trim() == "c" { break; }
        match id_string.trim().parse::<u32>() {
            Ok(value) => {
                if value == 0 || value > database.len() as u32 { 
                    output_text(&options, Output::Text("Not a valid id. Please try again or [c]ancel.".to_string()));
                    continue; 
                }
                f(database, &options, value)?; //return f
                break;
            },
            Err(_) => {
                output_text(&options, Output::Text("Not a valid id. Please try again or [c]ancel.".to_string()));
            }
        }
    }
    Ok(())
}

fn info_command(database: &mut Vec<Entry>, options: &Options, id: u32) -> io::Result<()> {
    output_text(&options, Output::Info(database[(id - 1) as usize].clone()));
    Ok(())
}

fn check_command(database: &mut Vec<Entry>, _options: &Options, id: u32) -> io::Result<()> {
    if database[(id - 1) as usize].checked == false { database[(id - 1) as usize].checked = true } else { database[(id - 1) as usize].checked = false }
    Ok(())
}

fn update_command(database: &mut Vec<Entry>, options: &Options, id: u32) -> io::Result<()> {
    output_text(&options, Output::Text("Please input the new description:".to_string()));
    let mut new_title = String::new();
    io::stdin().read_line(&mut new_title)?;
    database[(id - 1) as usize].title = new_title.trim().to_string();
    Ok(())
}

fn delete_command(database: &mut Vec<Entry>, _options: &Options, id: u32) -> io::Result<()> {
    database.remove((id - 1) as usize);
    let mut new_id = 1;
    for entry in database {
        entry.id = new_id;
        new_id += 1;
    }
    Ok(())
}

fn move_command(database: &mut Vec<Entry>, options: &Options) -> io::Result<()> {
    let old_id: u32;
    let mut new_id: u32;
    if (database.len() as u32) < 2 {
        output_text(&options, Output::Text("Moving with less than two items available is impossible.".to_string()));
        return Ok(());
    }
    output_text(&options, Output::Text("Please input the id of the item you want to move. ([c]ancel)".to_string()));
    loop {
        let mut id_string = String::new();
        io::stdin().read_line(&mut id_string)?;
        if id_string.trim() == "c" { return Ok(()); }
        match id_string.trim().parse::<u32>() {
            Ok(value) => {
                if value == 0 || value > database.len() as u32 { 
                    output_text(&options, Output::Text("Not a valid id. Please try again or [c]ancel.".to_string()));
                    continue; 
                }
                old_id = value;
                break;
            },
            Err(_) => {
                output_text(&options, Output::Text("Not a valid id. Please try again or [c]ancel.".to_string()));
            }
        }
    }
    output_text(&options, Output::Text("Please input the id the item shall have. ([c]ancel)".to_string()));
    loop {
        let mut id_string = String::new();
        io::stdin().read_line(&mut id_string)?;
        if id_string.trim() == "c" { return Ok(()); }
        match id_string.trim().parse::<u32>() {
            Ok(value) => {
                if value == 0 || value > database.len() as u32 { 
                    output_text(&options, Output::Text("Not a valid id. Please try again or [c]ancel.".to_string()));
                    continue; 
                }
                new_id = value;
                break;
            },
            Err(_) => {
                output_text(&options, Output::Text("Not a valid id. Please try again or [c]ancel.".to_string()));
            }
        }
    }

    let entry_to_move: Entry = database.remove((old_id - 1) as usize);
    database.insert((new_id - 1) as usize, entry_to_move);
    new_id = 1;
    for entry in database {
        entry.id = new_id;
        new_id += 1;
    }
    Ok(())
}

/// load_databes
/// loads a database in file format - micoto.csv
/// if this file is not found an empty database is created
/// a micoto.csv is a list of csv entries:
///   item,x
/// first item is the title of the entry of the list
/// second item is the checkmark (either an x or a whitespace)
fn load_database(options: &Options) -> io::Result<Vec<Entry>> {
    let mut database: Vec<Entry> = Vec::new();
    match fs::File::open("micoto.csv") {
        Ok(mut file) => {
            let mut entries_string = String::new();
            file.read_to_string(&mut entries_string)?;
            let mut line_number = 1;
            for line in entries_string.lines() {
                let mut split: Vec<&str> = line.split(',').collect();
                if split.len() < 2 { return Err(io::Error::new(io::ErrorKind::Other, "Database corrupted.")); }
                let checked = split.pop().unwrap() == "x";
                let mut title = split.join(",");
                parse_title_from_csv(&mut title);
                database.push(Entry{
                    id: line_number,
                    title: title,
                    checked: checked
                });
                line_number += 1;
            }
        },
        Err(_) => {
            output_text(&options, Output::Text("Could not find 'micoto.csv'. Starting with empty list.".to_string()));
        }
    }
    Ok(database)
}

fn save_database(database: &mut Vec<Entry>, options: &Options) {
    match fs::File::create("micoto.csv") {
        Ok(mut file) => {
            let mut file_content = String::new();
            for entry in database {
                let mut entry_clone = entry.clone();
                parse_title_to_csv(&mut entry_clone.title);
                file_content = file_content + &entry_clone.to_csv() + "\r\n";
            }
            let _ = file.write_all(file_content.as_bytes());
        },
        Err(_) => {
            output_text(&options, Output::Text("Could not find 'micoto.csv'. Creating one.".to_string()));
        }
    }
}

fn parse_title(title: &mut String, csv: bool) {
    if csv {
        if !title.contains(",") {
            return;
        }
        if title.contains("\"") {
            *title = title.replace("\"", "\"\"");
        }
        title.push('"');
        let return_title = String::from("\"");
        *title = return_title + &title;
        return;
    }
    if title.starts_with("\"") && title.ends_with("\"") && title.contains(",") { //edge case that someone has quotes at the beginning and the end without use of a comma
        title.remove(0);
        title.pop();
        *title = title.replace("\"\"", "\"");
        return;
    }
}


fn parse_title_to_csv(title: &mut String) {
    parse_title(title, true)
}

fn parse_title_from_csv(title: &mut String) {
    parse_title(title, false)
}

fn help_commands_explained(options: &Options) {
    output_text(&options, Output::Text("[n]ew - create a new list item by inputting its description.".to_string()));
    output_text(&options, Output::Text("[c]heck - mark or unmark a list item as done by inputting its id.".to_string()));
    output_text(&options, Output::Text("[u]pdate - change the description of a list item by inputting its id and then its new description.".to_string()));
    output_text(&options, Output::Text("[d]elete - delete a list item by inputting its id. The ids of the remaining items are updated accordingly.".to_string()));
    output_text(&options, Output::Text("[m]ove - moves a list item to that id by inputting its id. The ids of the remaining items are updated accordingly.".to_string()));
    output_text(&options, Output::Text("[v]iew - view the list of items.".to_string()));
    output_text(&options, Output::Text("[s]ave - save the list to micoto.csv.".to_string()));
    output_text(&options, Output::Text("[l]oad - load the list from micoto.csv. Beware: unsafed lists will be overwritten and all changes lost.".to_string()));
    output_text(&options, Output::Text("[h]elp - this helplisting.".to_string()));
    output_text(&options, Output::Text("[q]uit - quit the application after a fail safe prompt to save the list.".to_string()));
    if options.offset.is_some() { 
        output_text(&options, Output::Text("[p]age [n]umber - jump to a page number (e.g. p3) derived from the item count and the number given via commandline argument.".to_string()));
    }
}

fn help_commands(options: &Options) {
    let new = "[n]ew, ";
    let check = "[c]heck, ";
    let update = "[u]pdate, ";
    let delete = "[d]elete, ";
    let _move = "[m]ove, ";
    let view = "[v]iew, ";
    let save = "[s]ave, ";
    let load = "[l]oad, ";
    let help = "[h]elp, ";
    let quit = "[q]uit";
    let page = if options.offset.is_some() { "[p]age [x]number, " } else { "" };
    println!(
        "{}",
        format!(
            "Commands: {}{}{}{}{}{}{}{}{}{}{}",
            new,
            check,
            update,
            delete,
            _move,
            page,
            view,
            save,
            load,
            help,
            quit
        )
    );
}

fn show_list(database: &Vec<Entry>, options: &Options) {
    output_text(&options, Output::Text("Micoto - ToDo List".to_string()));
    let mut page_teller = String::from("-----");
    let mut _database = database.clone();
    if let Some(pages) = options.offset {
        page_teller = format!("----- Page {}/{} -----", pages + 1, (database.len() as u32 / options.page_length.unwrap()) + 1);
        _database = _database.split_at_mut((pages * options.page_length.unwrap()) as usize).1.to_vec();
        if _database.len() as u32 > options.page_length.unwrap() {
            _database = _database.split_at_mut(options.page_length.unwrap() as usize).0.to_vec();
        }
    }
    output_text(&options, Output::Text(page_teller));
    for entry in _database {
        output_text(&options, Output::Item(entry));
    }
}

fn truncate_text(width: u32, mut text: String) -> String {
    if width < text.chars().count() as u32 {
        text = text.chars().take((width - 3) as usize).collect::<String>();
        text.push_str("...");
    }
    text
}

fn output_text(options: &Options, output: Output) {
    match options.width {
        Some(width) => {
            match output {
                Output::Item(item) => {
                    let standard_item_format_wo_id_length = 9;//magic number: id " - " + " - [x]"
                    let mut _width = width - standard_item_format_wo_id_length; 
                    let mut _id = String::new();
                    if item.id < 1000 {
                        _id = format!("{:3}", item.id);
                        _width -= 3;
                    } else {
                        _id = item.id.to_string();
                        _width -= _id.len() as u32;
                    }
                    println!("{} - {} - [{}]", _id, truncate_text(_width, item.title.clone()), item.parse_checked());
                }
                Output::Text(text) => {
                    println!("{}", truncate_text(width, text));
                }
                Output::Info(item) => {
                    println!("ID: {}", item.id);
                    println!("Description:\r\n{}", item.title.clone());
                    println!("Done [{}]", item.parse_checked());
                }
                Output::Help => {
                    let mut commandhelp = "[h]elp, [n], [c], [u], [d], [m], [v], [s], [l], [q]".to_string();
                    if options.offset.is_some() { commandhelp.push_str(", [pX]"); }
                    println!("{}", truncate_text(width, commandhelp));
                }
            }
        }
        None => {
            match output {
                Output::Item(item) => {
                    item.to_cmd_string();
                }
                Output::Text(text) => {
                    println!("{}", text);
                }
                Output::Info(item) => {
                    println!("ID: {}", item.id);
                    println!("Description:\r\n{}", item.title.clone());
                    println!("Done [{}]", item.parse_checked());
                }
                Output::Help => {
                    help_commands(&options);
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_title_comma() {
        let mut test_string_w_comma_to = String::from("Test,");
        let mut test_string_w_comma_from = String::from("\"Test,\"");

        parse_title_to_csv(&mut test_string_w_comma_to);
        parse_title_from_csv(&mut test_string_w_comma_from);

        assert_eq!(String::from("\"Test,\""), test_string_w_comma_to);
        assert_eq!(String::from("Test,"), test_string_w_comma_from);
    }

    #[test]
    fn test_parse_title_wo_comma() {
        let mut test_string_wo_comma_to = String::from("test");
        let mut test_string_wo_comma_from = String::from("test");

        parse_title_to_csv(&mut test_string_wo_comma_to);
        parse_title_from_csv(&mut test_string_wo_comma_from);

        assert_eq!(String::from("test"), test_string_wo_comma_to);
        assert_eq!(String::from("test"), test_string_wo_comma_from);
    }

    #[test]
    fn test_parse_title_quote() {
        let mut test_string_wo_comma_w_quote_to = String::from("\"test\" test");
        let mut test_string_wo_comma_w_quote_from = String::from("\"test\" test");

        parse_title_to_csv(&mut test_string_wo_comma_w_quote_to);
        parse_title_from_csv(&mut test_string_wo_comma_w_quote_from);

        assert_eq!(String::from("\"test\" test"), test_string_wo_comma_w_quote_to);
        assert_eq!(String::from("\"test\" test"), test_string_wo_comma_w_quote_from);
    }

    #[test]
    fn test_parse_title_quote_front_end() {
        let mut test_string_wo_comma_quote_end_and_beg_to = String::from("\"test\" test \"test\"");
        let mut test_string_wo_comma_quote_end_and_beg_from = String::from("\"test\" test \"test\"");

        parse_title_to_csv(&mut test_string_wo_comma_quote_end_and_beg_to);
        parse_title_from_csv(&mut test_string_wo_comma_quote_end_and_beg_from);

        assert_eq!(String::from("\"test\" test \"test\""), test_string_wo_comma_quote_end_and_beg_to);
        assert_eq!(String::from("\"test\" test \"test\""), test_string_wo_comma_quote_end_and_beg_from);
    }

    #[test]
    fn test_parse_title_comma_quote_front_end() {
        let mut test_string_w_comma_quote_end_and_beg_to = String::from("\"test\",test,\"test\"");
        let mut test_string_w_comma_quote_end_and_beg_from = String::from("\"\"\"test\"\",test,\"\"test\"\"\"");

        parse_title_to_csv(&mut test_string_w_comma_quote_end_and_beg_to);
        parse_title_from_csv(&mut test_string_w_comma_quote_end_and_beg_from);

        assert_eq!(String::from("\"\"\"test\"\",test,\"\"test\"\"\""), test_string_w_comma_quote_end_and_beg_to);
        assert_eq!(String::from("\"test\",test,\"test\""), test_string_w_comma_quote_end_and_beg_from);
    }
}
