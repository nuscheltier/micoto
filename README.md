# Micoto - minimal commandline todolist
  
This is just a small tool developed in the spur of the moment. Nothing fancy about it.  
To use it just compile and start it and follow the instructions.
  
# Roadmap
  
* v1.1
  * pagination (optional, commandline parameter)
  * move items up and down
* v1.2
  * detailing an item
  * limit the width (optional, commandline parameter)
* v2.0
  * different lists
  
# License
No License granted. All rights reserved.
